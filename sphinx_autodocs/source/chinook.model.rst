chinook.model package
=====================

.. automodule:: chinook.model
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   chinook.model.orm_classes
