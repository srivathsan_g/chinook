chinook.queries package
=======================

.. automodule:: chinook.queries
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   chinook.queries.ddl_queries
   chinook.queries.dml_queries
   chinook.queries.dql_queries
   chinook.queries.pd_queries
