chinook package
===============

.. automodule:: chinook
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   chinook.model
   chinook.queries
   chinook.utils
