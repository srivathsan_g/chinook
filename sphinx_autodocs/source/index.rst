*****************
Chinook Database
*****************

.. toctree::
   :caption: Usage

   usage.rst

Developer documentation
************************

.. toctree::
   :caption: Entities
   :maxdepth: 15

   chinook.model.orm_classes

.. toctree::
   :caption: Queries
   :maxdepth: 15

   chinook.queries.ddl_queries
   chinook.queries.dml_queries
   chinook.queries.dql_queries
   chinook.queries.pd_queries

.. toctree::
   :caption: Helpers/Utilities
   :maxdepth: 15

   chinook.utils.query_helpers
   chinook.utils.log
   chinook.utils.error_handle


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
