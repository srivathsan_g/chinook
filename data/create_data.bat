@echo off
echo Chinook Database Version 1.4
echo.

set SQLFILE=create_data.sql

set MYSQL_PATH="C:\Program Files\MySQL\MySQL Server 8.0\bin\mysql.exe"

:RUNSQL
echo.
echo Running %SQLFILE%...
%MYSQL_PATH% -h localhost -u root --password=patterns <%SQLFILE%

:END
echo.
set SQLFILE=