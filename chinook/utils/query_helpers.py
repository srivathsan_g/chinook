# -- coding: UTF-8 --

"""
Query Helpers
==============
This module contains functions which help in the querying process.
"""

import logging
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine import Engine

from chinook.utils.error_handle import exception_handler, ERROR_DB_URL_TYPE, ERROR_ECHO_TYPE, ERROR_ENGINE_TYPE

__author__ = 'srivathsan@gyandata.com'

LOGGER=logging.getLogger(__name__)


@exception_handler
def create_eng(db_url, echo=False):
    """
    Function to create an engine.

    :param db_url: the database url to be connected.
    :type db_url: str

    :param echo: the logger echo
    :type echo: bool

    :return: the engine created
    :rtype: :class:`sqlalchemy.engine.Engine`
    """

    # parameter check
    if not issubclass(type(db_url), str):
        raise TypeError(ERROR_DB_URL_TYPE)

    if not issubclass(type(echo), bool):
        raise TypeError(ERROR_ECHO_TYPE)

    engine = create_engine(db_url, echo=echo, logging_name=__name__)

    LOGGER.debug("Engine created")

    return engine


@exception_handler
def get_session_factory(engine):
    """
    Function to create a session factory from the given engine.

    :param engine: the connected engine.
    :type engine: :class:`sqlalchemy.engine.Engine`

    :return: the session factory created.
    :rtype: :class:`sqlalchemy.orm.session.Session`
    """

    # parameter check
    if not isinstance(engine, Engine):
        raise TypeError(ERROR_ENGINE_TYPE)

    session_factory = sessionmaker(bind=engine)

    LOGGER.debug("Session factory created")

    return session_factory
