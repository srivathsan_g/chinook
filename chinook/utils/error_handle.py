# -- coding: UTF-8 --

"""
Error Handling
===============
The module which contains a function to be used as decorator for exception handling
and messages for error handling
"""

import functools
import logging
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound


__author__ = 'srivathsan@gyandata.com'

LOGGER=logging.getLogger(__name__)


def exception_handler(func):
    """
    A decorator function for func to implement entry and exit logging and exception handling.
    """

    @functools.wraps(func)
    def func_wrapper(*args, **kwargs):

        try:

            # entry and exit logging for function
            LOGGER.debug('Entering function %s', func.__name__)
            return func(*args, **kwargs)
            # LOGGER.debug('Exiting function %s', func.__name__)
            # if ret:
            # return ret

        except TypeError as ex:
            LOGGER.error('Type Error: %s', ex.args[0])

        except KeyError as ex:
            LOGGER.error('Key Error: %s', ex.args[0])

        except ValueError as ex:
            LOGGER.error('Value Error: %s', ex.args[0])

        except MultipleResultsFound as ex:
            LOGGER.error('MultipleResultsFound error: %s', ex.args[0])

        except NoResultFound as ex:
            LOGGER.error('NoResultFound Error: %s', ex.args[0])

        except Exception as ex:
            LOGGER.error('Error: %s', ex.args[0])

    return func_wrapper


###########################################################################################
# Error Messages
###########################################################################################

ERROR_PLAYLIST_ID_TYPE = "Given playlist id is not int"
"""
Error message to be displayed when given playlist id is not int
"""

ERROR_CUSTOMER_ID_TYPE = "Given customer id is not int"
"""
Error message to be displayed when given customer id is not int
"""

ERROR_GENRE_ID_TYPE = "Given genre id is not int"
"""
Error message to be displayed when given genre id is not int
"""

ERROR_EMPLOYEE_ID_TYPE = "Given employee id is not int"
"""
Error message to be displayed when given employee id is not int
"""

ERROR_USER_TYPE = "Given user is not str"
"""
Error message to be displayed when given user is not str
"""

ERROR_MEDIA_ID_TYPE = "Given media type id is not int"
"""
Error message to be displayed when given media type id is not int
"""

ERROR_PRICE_TYPE = "Given price is not int or float"
"""
Error message to be displayed when given price is not int or float
"""

ERROR_NO_TYPE = "Given number is not int"
"""
Error message to be displayed when given number is not int
"""

ERROR_DB_URL_TYPE = "Given db url is not str"
"""
Error message to be displayed when given db url is not str
"""

ERROR_ECHO_TYPE = "Given echo is not bool"
"""
Error message to be displayed when given echo is not bool
"""

ERROR_ENGINE_TYPE = "Given engine is not instance of Engine"
"""
Error message to be displayed when given engine is not instance of Engine
"""
