# -- coding: UTF-8 --

"""
ORM Classes
============
This module contains the ORM classes for the Chinook Database
"""

from sqlalchemy import ForeignKey, Column, Integer, \
    Numeric, Boolean, DateTime, TIMESTAMP, NVARCHAR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.sql import expression
from sqlalchemy.sql import text
from sqlalchemy.sql.functions import now

__author__ = 'srivathsan@gyandata.com'

Base = declarative_base()


class UserTrailMixIn:
    """
    Class which is used as Mixin class by all ORM classes to have attributes which track the User Trail

    :ivar created_by: this holds the name of user (default system) who created the record.
    :vartype created_by: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar created_on: this holds the timestamp of when the record was created.
    :vartype created_on: :class:`sqlalchemy.dialects.mysql.TIMESTAMP`

    :ivar last_updated_by: this holds the last user who updated the record.
    :vartype last_updated_by: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar created_on: this holds the timestamp of when the record was updated last.
    :vartype created_on: :class:`sqlalchemy.dialects.mysql.TIMESTAMP`

    :ivar is_enabled: this is to set the record as available, if deleted it is false
    :vartype is_enabled: :class:`sqlalchemy.dialects.mysql.Boolean`
    """

    __table_args__ = {"mysql_engine": "InnoDB"}

    created_by = Column(NVARCHAR(255), nullable=False, server_default="SYSTEM")

    created_on = Column(TIMESTAMP, nullable=False, server_default=now())

    last_updated_by = Column(NVARCHAR(255), nullable=True, server_default="SYSTEM")

    last_updated_on = Column(TIMESTAMP, nullable=True,
                             server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))

    is_enabled = Column(Boolean, nullable=False, server_default=expression.true())


class Album(UserTrailMixIn, Base):
    """
    ORM Class to represent album table.

    :ivar AlbumId: the primary key of the table
    :vartype AlbumId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar Title: the title of the album
    :vartype Title: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar ArtistId: the foreign key for the artist of the album
    :vartype ArtistId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar artist: the artist of the album
    :vartype artist: :class:`chinook.model.orm_classes.Artist`

    :ivar tracks: the tracks of this album
    :vartype tracks: :class:`chinook.model.orm_classes.Track`
    """
    __tablename__ = "album"

    AlbumId = Column(Integer, primary_key=True, nullable=False, autoincrement=True)

    Title = Column(NVARCHAR(160), nullable=False)

    ArtistId = Column(Integer, ForeignKey("artist.ArtistId"), nullable=False)

    artist = relationship("Artist", back_populates="albums", uselist=False)

    tracks = relationship("Track", back_populates="album", cascade="all,delete,delete-orphan")


class Artist(UserTrailMixIn, Base):
    """
    ORM class to represent artist table.

    :ivar ArtistId: the primary key of the table
    :vartype ArtistId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar Name: the name of the album
    :vartype Name: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar albums: the albums done by this artist
    :vartype albums: :class:`chinook.model.orm_classes.Album`
    """
    __tablename__ = "artist"

    ArtistId = Column(Integer, primary_key=True, nullable=False, autoincrement=True)

    Name = Column(NVARCHAR(120))

    albums = relationship("Album", back_populates="artist", cascade="all,delete,delete-orphan")


class Customer(UserTrailMixIn, Base):
    """
    ORM class to represent the customer table.

    :ivar CustomerId: the primary key of the table
    :vartype CustomerId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar FirstName: the first name of customer
    :vartype FirstName: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar LastName: the last name of customer
    :vartype LastName: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar Company: the company of the customer
    :vartype Company: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar Address: the address of the customer
    :vartype Address: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar City: the city of the customer
    :vartype City: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar State: the state of the customer
    :vartype State: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar Country: the country of the customer
    :vartype Country: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar PostalCode: the postal code of the customer
    :vartype PostalCode: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar Phone: the phone number of the customer
    :vartype Phone: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar Fax: the fax of the customer
    :vartype Fax: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar Email: the email of the customer
    :vartype Email: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar SupportRepId: the foreign key id of the employee who supports the customer
    :vartype SupportRepId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar support_employee: the employee who supports the customer
    :vartype support_employee: :class:`chinook.model.orm_classes.Employee`

    :ivar invoices: the invoices made by the customer
    :vartype invoices: :class:`chinook.model.orm_classes.Invoice`
    """
    __tablename__ = "customer"

    CustomerId = Column(Integer, primary_key=True, nullable=False, autoincrement=True)

    FirstName = Column(NVARCHAR(40), nullable=False)

    LastName = Column(NVARCHAR(20), nullable=False)

    Company = Column(NVARCHAR(80))

    Address = Column(NVARCHAR(70))

    City = Column(NVARCHAR(40))

    State = Column(NVARCHAR(40))

    Country = Column(NVARCHAR(40))

    PostalCode = Column(NVARCHAR(10))

    Phone = Column(NVARCHAR(24))

    Fax = Column(NVARCHAR(24))

    Email = Column(NVARCHAR(60), nullable=False)

    SupportRepId = Column(Integer, ForeignKey("employee.EmployeeId"))

    support_employee = relationship("Employee", back_populates="support_customers",
                                    uselist=False)

    invoices = relationship("Invoice", back_populates="customer", cascade="all,delete,delete-orphan")


class Employee(UserTrailMixIn, Base):
    """
    ORM class to represent the employee table.

    :ivar EmployeeId: the primary key of the table
    :vartype EmployeeId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar FirstName: the first name of employee
    :vartype FirstName: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar LastName: the last name of employee
    :vartype LastName: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar Title: the title of the employee
    :vartype Title: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar ReportsTo: the foreign key id of the employee to whom this employee reports
    :vartype ReportsTo: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar BirthDate: the dob of the employee
    :vartype BirthDate: :class:`sqlalchemy.dialects.mysql.DateTime`

    :ivar HireDate: the doj of the employee
    :vartype HireDate: :class:`sqlalchemy.dialects.mysql.DateTime`

    :ivar Address: the address of the employee
    :vartype Address: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar City: the city of the employee
    :vartype City: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar State: the state of the employee
    :vartype State: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar Country: the country of the employee
    :vartype Country: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar PostalCode: the postal code of the employee
    :vartype PostalCode: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar Phone: the phone number of the employee
    :vartype Phone: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar Fax: the fax of the employee
    :vartype Fax: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar Email: the email of the employee
    :vartype Email: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar support_customers: the customers supported by this employee
    :vartype support_customers: :class:`chinook.model.orm_classes.Customer`

    :ivar report_to: the manager employee
    :vartype report_to: :class:`chinook.model.orm_classes.Employee`
    """
    __tablename__ = "employee"

    EmployeeId = Column(Integer, primary_key=True, nullable=False, autoincrement=True)

    FirstName = Column(NVARCHAR(20), nullable=False)

    LastName = Column(NVARCHAR(20), nullable=False)

    Title = Column(NVARCHAR(30))

    ReportsTo = Column(Integer, ForeignKey("employee.EmployeeId"))

    BirthDate = Column(DateTime)

    HireDate = Column(DateTime)

    Address = Column(NVARCHAR(70))

    City = Column(NVARCHAR(40))

    State = Column(NVARCHAR(40))

    Country = Column(NVARCHAR(40))

    PostalCode = Column(NVARCHAR(10))

    Phone = Column(NVARCHAR(24))

    Fax = Column(NVARCHAR(24))

    Email = Column(NVARCHAR(60))

    support_customers = relationship("Customer", back_populates="support_employee",
                                     cascade="all,delete,delete-orphan")

    report_to = relationship("Employee", backref="subordinates",
                             uselist=False, remote_side="Employee.EmployeeId")


class Genre(UserTrailMixIn, Base):
    """
    ORM class to represent genre table.

    :ivar GenreId: the primary key of the table
    :vartype GenreId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar Name: the name of the genre
    :vartype Name: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar tracks: the tracks in this genre
    :vartype tracks: :class:`chinook.model.orm_classes.Track`
    """
    __tablename__ = "genre"

    GenreId = Column(Integer, primary_key=True, nullable=False, autoincrement=True)

    Name = Column(NVARCHAR(120))

    tracks = relationship("Track", back_populates="genre", cascade="all,delete,delete-orphan")


class Invoice(UserTrailMixIn, Base):
    """
    ORM class to represent the invoice table.

    :ivar InvoiceId: the primary key of the table
    :vartype InvoiceId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar CustomerId: the foreign key id of the customer who is involved in the invoice
    :vartype CustomerId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar InvoiceDate: the date of the invoice
    :vartype InvoiceDate: :class:`sqlalchemy.dialects.mysql.DateTime`

    :ivar BillingAddress: the address of the invoice
    :vartype BillingAddress: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar BillingCity: the city of the invoice
    :vartype BillingCity: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar BillingState: the state of the invoice
    :vartype BillingState: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar BillingCountry: the country of the invoice
    :vartype BillingCountry: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar BillingPostalCode: the postal code of the invoice
    :vartype BillingPostalCode: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar Total: the total bill of the invoice
    :vartype Total: :class:`sqlalchemy.dialects.mysql.Numeric`

    :ivar customer: the customer involved in the invoice
    :vartype customer: :class:`chinook.model.orm_classes.Customer`

     :ivar tracks: the tracks involved in the invoice
    :vartype tracks: :class:`chinook.model.orm_classes.Track`
    """
    __tablename__ = "invoice"

    InvoiceId = Column(Integer, primary_key=True, nullable=False, autoincrement=True)

    CustomerId = Column(Integer, ForeignKey("customer.CustomerId"), nullable=False)

    InvoiceDate = Column(DateTime, nullable=False)

    BillingAddress = Column(NVARCHAR(70))

    BillingCity = Column(NVARCHAR(40))

    BillingState = Column(NVARCHAR(40))

    BillingCountry = Column(NVARCHAR(40))

    BillingPostalCode = Column(NVARCHAR(10))

    Total = Column(Numeric(10, 2), nullable=False)

    customer = relationship("Customer", back_populates="invoices", uselist=False)

    tracks = relationship("Track", secondary="invoiceline", back_populates="invoice_lines")


class InvoiceLine(UserTrailMixIn, Base):
    """
    ORM class to represent secondary association table invoiceline.

    :ivar InvoiceLineId: the primary key of the table
    :vartype InvoiceLineId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar InvoiceId: the foreign key id of the invoice
    :vartype InvoiceId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar TrackId: the foreign key id of the track
    :vartype TrackId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar UnitPrice: the unit price of the track
    :vartype UnitPrice: :class:`sqlalchemy.dialects.mysql.Numeric`

    :ivar Quantity: the number of tracks
    :vartype Quantity: :class:`sqlalchemy.dialects.mysql.Integer`
    """
    __tablename__ = "invoiceline"

    InvoiceLineId = Column(Integer, primary_key=True, nullable=False, autoincrement=True)

    InvoiceId = Column(Integer, ForeignKey("invoice.InvoiceId"), nullable=False)

    TrackId = Column(Integer, ForeignKey("track.TrackId"), nullable=False)

    UnitPrice = Column(Numeric(10, 2), nullable=False)

    Quantity = Column(Integer, nullable=False)


class MediaType(UserTrailMixIn, Base):
    """
    ORM class to represent mediatype table.

    :ivar MediaTypeId: the primary key of the table
    :vartype MediaTypeId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar Name: the name of the media
    :vartype Name: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar tracks: the tracks in this media
    :vartype tracks: :class:`chinook.model.orm_classes.Track`
    """
    __tablename__ = "mediatype"

    MediaTypeId = Column(Integer, primary_key=True, nullable=False, autoincrement=True)

    Name = Column(NVARCHAR(120))

    tracks = relationship("Track", back_populates="media_type", cascade="all,delete,delete-orphan")


class Playlist(UserTrailMixIn, Base):
    """
    ORM class to represent playlist table.

    :ivar PlaylistId: the primary key of the table
    :vartype PlaylistId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar Name: the name of the playlist
    :vartype Name: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar tracks: the tracks in this playlist
    :vartype tracks: :class:`chinook.model.orm_classes.Track`
    """
    __tablename__ = "playlist"

    PlaylistId = Column(Integer, primary_key=True, nullable=False, autoincrement=True)

    Name = Column(NVARCHAR(120))

    tracks = relationship("Track", secondary="playlisttrack", back_populates="playlists", cascade="all,delete")


class PlaylistTrack(UserTrailMixIn, Base):
    """
    ORM class to represent the secondary association table playlisttrack

    :ivar PlaylistId: the foreign key id of the playlist
    :vartype InvoiceId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar TrackId: the foreign key id of the track
    :vartype TrackId: :class:`sqlalchemy.dialects.mysql.Integer`
    """
    __tablename__ = "playlisttrack"

    PlaylistId = Column(Integer, ForeignKey("playlist.PlaylistId"),
                        primary_key=True, nullable=False)

    TrackId = Column(Integer, ForeignKey("track.TrackId"),
                     primary_key=True, nullable=False)


class Track(UserTrailMixIn, Base):
    """
    ORM class to represent track table.

    :ivar TrackId: the primary key of the table
    :vartype TrackId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar Name: the name of the track
    :vartype Name: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar AlbumId: the foreign key id of the album
    :vartype AlbumId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar MediaTypeId: the foreign key id of the media type
    :vartype MediaTypeId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar GenreId: the foreign key id of the genre
    :vartype GenreId: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar Composer: the composer of the track
    :vartype Composer: :class:`sqlalchemy.dialects.mysql.NVARCHAR`

    :ivar Milliseconds: the duration of the track in milliseconds
    :vartype Milliseconds: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar Bytes: the size of the track in bytes
    :vartype Bytes: :class:`sqlalchemy.dialects.mysql.Integer`

    :ivar UnitPrice: the price of the track
    :vartype UnitPrice: :class:`sqlalchemy.dialects.mysql.Numeric`

    :ivar album: the album of the track
    :vartype album: :class:`chinook.model.orm_classes.Album`

    :ivar media_type: the media type of the track
    :vartype media_type: :class:`chinook.model.orm_classes.MediaType`

    :ivar genre: the genre of the track
    :vartype genre: :class:`chinook.model.orm_classes.Genre`

    :ivar invoice_lines: the invoices involved by the track
    :vartype invoice_lines: :class:`chinook.model.orm_classes.Invoice`

    :ivar playlists: the playlists that have this track
    :vartype playlists: :class:`chinook.model.orm_classes.Playlist`
    """
    __tablename__ = "track"

    TrackId = Column(Integer, primary_key=True, nullable=False, autoincrement=True)

    Name = Column(NVARCHAR(200), nullable=False)

    AlbumId = Column(Integer, ForeignKey("album.AlbumId"), nullable=False)

    MediaTypeId = Column(Integer, ForeignKey("mediatype.MediaTypeId"), nullable=False)

    GenreId = Column(Integer, ForeignKey("genre.GenreId"))

    Composer = Column(NVARCHAR(220))

    Milliseconds = Column(Integer, nullable=False)

    Bytes = Column(Integer)

    UnitPrice = Column(Numeric(10, 2), nullable=False)

    album = relationship("Album", back_populates="tracks", uselist=False)

    media_type = relationship("MediaType", back_populates="tracks", uselist=False)

    genre = relationship("Genre", back_populates="tracks", uselist=False)

    invoice_lines = relationship("Invoice", secondary="invoiceline", back_populates="tracks", cascade="all,delete")

    playlists = relationship("Playlist", secondary="playlisttrack", back_populates="tracks")
