# -- coding: UTF-8 --

"""
DDL Queries
============
This module contains queries to the chinook database which change the definition of the tables
"""
import logging

from sqlalchemy.sql import text
from sqlalchemy.engine import Engine

from chinook.utils.error_handle import exception_handler, ERROR_ENGINE_TYPE

__author__ = 'srivathsan@gyandata.com'

LOGGER = logging.getLogger(__name__)


@exception_handler
def alter_del_cascade(engine):
    """
    Function to alter the table foreign key constraints to have on delete cascade

    :param engine: the connected engine.
    :type engine: :class:`sqlalchemy.engine.Engine`

    :return: None
    :rtype: None
    """

    # parameter check
    if not isinstance(engine, Engine):
        raise TypeError(ERROR_ENGINE_TYPE)

    LOGGER.info("Selecting all foreign key constraints from the chinook database")

    # get sql statement as text
    stmt = text("SELECT TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME FROM "
                "INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_SCHEMA = 'chinook';")

    # execute the sql statement to get all constraints
    conn = engine.connect()
    constraints = conn.execute(stmt)

    # constraints cannot be altered and must be dropped and recreated
    # and DDL queries can only be executed in textual format using engine connection

    # for each constraint drop it and create a new constraint with on delete cascade
    for constraint in constraints:
        table_name = constraint[0]
        column_name = constraint[1]
        constraint_name = constraint[2]
        reference_table = constraint[3]
        reference_column = constraint[4]

        LOGGER.info("For %s", table_name)

        # drop existing constraint
        drop_const = text(f"ALTER TABLE {table_name} DROP FOREIGN KEY `{constraint_name}`;")

        LOGGER.debug("Dropping %s", constraint_name)

        conn.execute(drop_const)

        # create new constraint with cascade
        cascade_const = text(f"ALTER TABLE {table_name} ADD CONSTRAINT `{constraint_name}` FOREIGN KEY (`"
                             f"{column_name}` ) REFERENCES `{reference_table}` (`{reference_column}` ) "
                             f"ON DELETE CASCADE;")

        LOGGER.debug("Creating %s", constraint_name)

        conn.execute(cascade_const)
