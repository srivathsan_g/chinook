# -- coding: UTF-8 --

"""
DQL Queries
============
This module contains queries to fetch data from the chinook database tables.
"""
import logging
import datetime
import calendar

from sqlalchemy import desc
from sqlalchemy.sql import text, any_, and_
from sqlalchemy.orm import aliased
from sqlalchemy.sql.functions import sum as sql_sum, count, func

from chinook.model.orm_classes import Track, Playlist, PlaylistTrack, \
    InvoiceLine, Invoice, Customer, Album, Artist, Genre, Employee
from chinook.utils.error_handle import exception_handler, ERROR_PLAYLIST_ID_TYPE, \
    ERROR_CUSTOMER_ID_TYPE, ERROR_NO_TYPE

__author__ = 'srivathsan@gyandata.com'

LOGGER = logging.getLogger(__name__)


@exception_handler
def get_all_tracks(session):
    """
    Function to fetch all the tracks.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :return: None
    :rtype: None
    """

    # queries track for all records
    LOGGER.debug('Query Track')
    tracks = session.query(Track). \
        filter(Track.is_enabled == True).all()

    print_str = "\n"
    for track in tracks:
        print_str += " ".join(map(str, [track.TrackId, track.Name, track.album.Title, track.genre.Name,
                                        track.media_type.Name]))
        print_str += "\n"

    LOGGER.info(print_str)


@exception_handler
def get_tracks_of_playlists(session, playlist_id):
    """
    Function to fetch all tracks given the playlist.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param playlist_id: the playlist id
    :type playlist_id: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(playlist_id), int):
        raise TypeError(ERROR_PLAYLIST_ID_TYPE)

    # check if playlist id is valid
    _ = session.query(Playlist). \
        filter(Playlist.PlaylistId == playlist_id).one()

    # queries track by joining with playlisttrack, playlist and filter based on playlist id
    LOGGER.debug("Query tracks given playlist")
    tracks = session.query(Track). \
        join(PlaylistTrack). \
        join(Playlist). \
        filter(Playlist.PlaylistId == playlist_id, Track.is_enabled == True).all()

    print_str = "\n"
    for track in tracks:
        print_str += " ".join(map(str, [track.TrackId, track.Name, track.album.Title, track.genre.Name,
                                        track.media_type.Name]))
        print_str += "\n"

    LOGGER.info(print_str)


@exception_handler
def get_tracks_of_customer(session, customer_id):
    """
    Function to fetch all tracks given the customer.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param customer_id: the customer id
    :type customer_id: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(customer_id), int):
        raise TypeError(ERROR_CUSTOMER_ID_TYPE)

    # check if customer id is valid
    _ = session.query(Customer). \
        filter(Customer.CustomerId == customer_id).one()

    # queries track by joining with invoiceline, invoice and filter based on customer id
    LOGGER.debug("Query tracks given customer")
    tracks = session.query(Track). \
        join(InvoiceLine). \
        join(Invoice). \
        filter(Invoice.CustomerId == customer_id, Track.is_enabled == True).all()

    print_str = "\n"
    for track in tracks:
        print_str += " ".join(map(str, [track.TrackId, track.Name, track.album.Title, track.genre.Name,
                                        track.media_type.Name]))
        print_str += "\n"

    LOGGER.info(print_str)


@exception_handler
def get_playlists_having_tracks_bought_by_customer(session, customer_id):
    """
    Function to fetch all playlists which contain tracks bought by given customer.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param customer_id: the customer id
    :type customer_id: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(customer_id), int):
        raise TypeError(ERROR_CUSTOMER_ID_TYPE)

    # check if customer id is valid
    _ = session.query(Customer). \
        filter(Customer.CustomerId == customer_id).one()

    # queries playlist by joining with playlisttrack, track, invoiceline, invoice
    # and filter based on customer id

    LOGGER.debug("Query playlist given customer")
    playlists = session.query(Playlist). \
        join(PlaylistTrack). \
        join(Track). \
        join(InvoiceLine). \
        join(Invoice). \
        filter(Invoice.CustomerId == customer_id, Playlist.is_enabled == True).all()

    print_str = "\n"
    for playlist in playlists:
        print_str += " ".join(map(str, [playlist.PlaylistId, playlist.Name]))
        print_str += "\n"

    LOGGER.info(print_str)


@exception_handler
def get_top_selling_tracks(session, n_songs):
    """
    Function to get the top n selling songs.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param n_songs: the top selling number of songs to get
    :type n_songs: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(n_songs), int):
        raise TypeError(ERROR_NO_TYPE)

    tracks = session.query(Track.Name.label("Song"), sql_sum(InvoiceLine.UnitPrice).label("Total")). \
        select_from(Track) \
        .join(InvoiceLine).filter(InvoiceLine.is_enabled == True). \
        group_by(Track.TrackId). \
        order_by(desc(text("Total")), Track.TrackId). \
        limit(n_songs).all()

    print_str = "\n"
    for track in tracks:
        print_str += " ".join(map(str, track))
        print_str += "\n"

    LOGGER.info(print_str)


@exception_handler
def get_top_albums_on_tracks(session, n_albums):
    """
    Function to get the top n albums based on the number of tracks.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param n_albums: the top albums based on the number of tracks to get
    :type n_albums: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(n_albums), int):
        raise TypeError(ERROR_NO_TYPE)

    albums = session.query(Album.Title.label("Album"), count(Track.TrackId).label("No_Songs")). \
        select_from(Album) \
        .join(Track).filter(Track.is_enabled == True). \
        group_by(Album.AlbumId). \
        order_by(desc(text("No_Songs")), Album.AlbumId). \
        limit(n_albums).all()

    print_str = "\n"
    for album in albums:
        print_str += " ".join(map(str, album))
        print_str += "\n"

    LOGGER.info(print_str)


@exception_handler
def get_top_artists_on_tracks(session, n_artists):
    """
    Function to get the top n Artists based on the number of tracks.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param n_artists: the top Artists based on the number of tracks to get
    :type n_artists: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(n_artists), int):
        raise TypeError(ERROR_NO_TYPE)

    artists = session.query(Artist.Name.label("Artist"), count(Track.TrackId).label("No_Songs")). \
        select_from(Artist) \
        .join(Album). \
        join(Track).filter(Track.is_enabled == True). \
        group_by(Artist.ArtistId). \
        order_by(desc(text("No_Songs")), Artist.ArtistId). \
        limit(n_artists).all()

    print_str = "\n"
    for artist in artists:
        print_str += " ".join(map(str, artist))
        print_str += "\n"

    LOGGER.info(print_str)


@exception_handler
def get_top_cust_on_amount(session, n_cust):
    """
    Function to get the top n Customers based on the total amount in the purchase history.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param n_cust: the top Customers based on the total amount in the purchase history to get
    :type n_cust: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(n_cust), int):
        raise TypeError(ERROR_NO_TYPE)

    customers = session.query(Customer.FirstName.label("Customer"), sql_sum(Invoice.Total).label("Amount")). \
        select_from(Customer) \
        .join(Invoice) \
        .filter(Invoice.is_enabled == True). \
        group_by(Customer.CustomerId). \
        order_by(desc(text("Amount")), Customer.CustomerId). \
        limit(n_cust).all()

    print_str = "\n"
    for customer in customers:
        print_str += " ".join(map(str, customer))
        print_str += "\n"

    LOGGER.info(print_str)


@exception_handler
def get_top_albums_on_purchase(session, n_albums):
    """
    Function to get the top n Albums based on the number of purchases.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param n_albums: the top Albums based on the number of purchases to get
    :type n_albums: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(n_albums), int):
        raise TypeError(ERROR_NO_TYPE)

    purchase = session.query(Track.TrackId). \
        join(InvoiceLine).filter(InvoiceLine.is_enabled == True)

    albums = session.query(Album.Title.label("Album"), count(Track.TrackId).label("No_Purchases")). \
        select_from(Album) \
        .join(Track). \
        filter(Track.TrackId == any_(purchase)). \
        group_by(Album.AlbumId). \
        order_by(desc(text("No_Purchases")), Album.AlbumId). \
        limit(n_albums).all()

    print_str = "\n"
    for album in albums:
        print_str += " ".join(map(str, album))
        print_str += "\n"

    LOGGER.info(print_str)


#@exception_handler
def get_tracks_on_purch_each_genre(session, n_songs):
    """
    Function to get the top n Tracks for each Genre based on the number of purchases.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param n_songs: the top Tracks for each Genre based on the number of purchases to get
    :type n_songs: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(n_songs), int):
        raise TypeError(ERROR_NO_TYPE)

    genre_ranked = session.query(Genre.Name.label("Genre_Name"), Track.Name.label("Song"),
                                 count(InvoiceLine.InvoiceLineId).label("No_Purchases"),
                                 func.row_number().
                                 over(partition_by=Track.GenreId,
                                      order_by=[Track.GenreId,
                                                desc(count(InvoiceLine.InvoiceLineId)),
                                                Track.TrackId]).label("genre_rank")). \
        select_from(Genre). \
        join(Track).join(InvoiceLine).\
        group_by(Track.TrackId).subquery()

    tracks = session.query(genre_ranked.c.Genre_Name, genre_ranked.c.Song, genre_ranked.c.No_Purchases).\
        filter(text(f"genre_rank<={n_songs}")).all()

    print_str = "\n"
    for track in tracks:
        print_str += " ".join(map(str, track))
        print_str += "\n"

    LOGGER.info(print_str)


@exception_handler
def get_top_tracks_playtime(session, n_songs):
    """
    Function to get the top n longest tracks based on playtime of the track.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param n_songs: the top longest tracks based on playtime of the track to get
    :type n_songs: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(n_songs), int):
        raise TypeError(ERROR_NO_TYPE)

    tracks = session.query(Track.Name.label("Song"), Track.Milliseconds / 1000). \
        select_from(Track) \
        .order_by(desc(Track.Milliseconds)). \
        limit(n_songs).all()

    print_str = "\n"
    for track in tracks:
        print_str += " ".join(map(str, track))
        print_str += "\n"

    LOGGER.info(print_str)


@exception_handler
def get_top_albums_tracks_playtime(session, n_albums):
    """
    Function to get the top n longest tracks based on playtime of the track.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param n_albums: the top longest tracks based on playtime of the track to get
    :type n_albums: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(n_albums), int):
        raise TypeError(ERROR_NO_TYPE)

    albums = session.query(Album.Title.label("Album name"), sql_sum(Track.Milliseconds / 1000).label(
        "Album_playtime")). \
        select_from(Album). \
        join(Track). \
        group_by(Album.AlbumId) \
        .order_by(desc(text("Album_playtime"))). \
        limit(n_albums).all()

    print_str = "\n"
    for album in albums:
        print_str += " ".join(map(str, album))
        print_str += "\n"

    LOGGER.info(print_str)


@exception_handler
def get_no_playlist_for_track(session, track_id):
    """
    Function to get the Number of playlists a track has been added to.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param track_id: the Number of playlists a track has been added to
    :type track_id: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(track_id), int):
        raise TypeError(ERROR_NO_TYPE)

    no_playlists = session.query(count(PlaylistTrack.PlaylistId)). \
        select_from(PlaylistTrack). \
        join(Track). \
        filter(Track.TrackId == track_id). \
        one()

    print_str = "\n" + str(no_playlists)

    LOGGER.info(print_str)


@exception_handler
def get_no_playlist_for_album(session, album_id):
    """
    Function to get the Number of playlists an album has been added to.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param album_id: the Number of playlists an album has been added to
    :type album_id: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(album_id), int):
        raise TypeError(ERROR_NO_TYPE)

    no_playlists = session.query(count(PlaylistTrack.PlaylistId)). \
        select_from(PlaylistTrack). \
        join(Track). \
        join(Album). \
        filter(Album.AlbumId == album_id). \
        group_by(Album.AlbumId, PlaylistTrack.PlaylistId). \
        count()

    print_str = "\n" + str(no_playlists)

    LOGGER.info(print_str)


@exception_handler
def genre_check_for_tracks(session):
    """
    Function to check if tracks that are associated with more than 1 genre.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :return: None
    :rtype: None
    """

    no_genre = session.query(count(Track.GenreId)). \
        select_from(Track). \
        join(Genre). \
        group_by(Track.TrackId). \
        all()

    genre_check = "None" if all(i[0] == 1 for i in no_genre) else "Error"

    print_str = "\n" + str(genre_check)

    LOGGER.info(print_str)


@exception_handler
def get_no_artist_for_dist_genre(session, n_artists):
    """
    Function to get the top artists who have the most number of distinct genres in their discography.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param n_artists: the Number of artists who have the most number of distinct genres in their discography
    :type n_artists: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(n_artists), int):
        raise TypeError(ERROR_NO_TYPE)

    artists = session.query(Artist.Name.label("Artist"), count(Genre.GenreId).label("No_Genres")). \
        select_from(Artist) \
        .join(Album). \
        join(Track). \
        join(Genre).filter(Genre.is_enabled == True). \
        group_by(Artist.ArtistId, Genre.GenreId). \
        order_by(desc(text("No_Genres")), Artist.ArtistId). \
        limit(n_artists).all()

    print_str = "\n"
    for artist in artists:
        print_str += " ".join(map(str, artist))
        print_str += "\n"

    LOGGER.info(print_str)


@exception_handler
def get_top_employee_sales(session, n_emp, month, year):
    """
    Function to get the top Employees based on the number of sales in any given month.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param n_emp: the Number Employees based on the number of sales in any given month
    :type n_emp: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(n_emp), int):
        raise TypeError(ERROR_NO_TYPE)

    if month in calendar.month_name:
        mon_fmt = "%B"
    elif month in calendar.month_abbr:
        mon_fmt = "%b"
    else:
        mon_fmt = "%m"

    yr_fmt = "%Y" if len(str(year)) == 4 else "%y"

    start_date = datetime.datetime.strptime(f"{year}-{month}-01", f"{yr_fmt}-{mon_fmt}-%d")
    int_month = int(start_date.strftime('%m'))
    end_date = datetime.datetime.strptime(f"{year}-{month}-{calendar.monthrange(year, int_month)[1]}",
                                          f"{yr_fmt}-{mon_fmt}-%d")

    emps = session.query(Employee.FirstName.label("Employee"), count(Invoice.InvoiceId).label("Sales")). \
        select_from(Employee). \
        join(Customer) \
        .join(Invoice) \
        .filter(and_(Invoice.is_enabled == True, Invoice.InvoiceDate >= start_date, Invoice.InvoiceDate <= end_date)). \
        group_by(Employee.EmployeeId). \
        order_by(desc(text("Sales")), Employee.EmployeeId). \
        limit(n_emp).all()

    print_str = "\n"
    for emp in emps:
        print_str += " ".join(map(str, emp))
        print_str += "\n"

    LOGGER.info(print_str)


@exception_handler
def get_top_manager_sales(session, n_emp, month, year):
    """
    Function to get the top managers based on the total sales in any given month.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param n_emp: the Number managers based on the total sales in any given month
    :type n_emp: int

    :return: None
    :rtype: None
    """

    # parameter check
    if not issubclass(type(n_emp), int):
        raise TypeError(ERROR_NO_TYPE)

    if month in calendar.month_name:
        mon_fmt = "%B"
    elif month in calendar.month_abbr:
        mon_fmt = "%b"
    else:
        mon_fmt = "%m"

    yr_fmt = "%Y" if len(str(year)) == 4 else "%y"

    start_date = datetime.datetime.strptime(f"{year}-{month}-01", f"{yr_fmt}-{mon_fmt}-%d")
    int_month = int(start_date.strftime('%m'))
    end_date = datetime.datetime.strptime(f"{year}-{month}-{calendar.monthrange(year, int_month)[1]}",
                                          f"{yr_fmt}-{mon_fmt}-%d")

    Employee_alias = aliased(Employee)

    emps = session.query(Employee_alias.FirstName.label("Manager"), sql_sum(Invoice.Total).label("Sales")). \
        select_from(Employee_alias). \
        join(Employee, Employee_alias.EmployeeId==Employee.ReportsTo). \
        join(Customer) \
        .join(Invoice) \
        .filter(and_(Invoice.is_enabled == True, Invoice.InvoiceDate >= start_date, Invoice.InvoiceDate <= end_date)). \
        group_by(Employee_alias.EmployeeId). \
        order_by(desc(text("Sales")), Employee_alias.EmployeeId). \
        limit(n_emp).all()

    print_str = "\n"
    for emp in emps:
        print_str += " ".join(map(str, emp))
        print_str += "\n"

    LOGGER.info(print_str)
