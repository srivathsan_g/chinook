# -- coding: UTF-8 --

"""
PD Queries
===========
This module contains functions which return a pandas dataframe from the data fetched.
"""

import logging
import pandas as pd

from chinook.model.orm_classes import Track, Album, Artist, Genre
from chinook.utils.error_handle import exception_handler

__author__ = 'srivathsan@gyandata.com'

LOGGER = logging.getLogger(__name__)


@exception_handler
def pd_album_genre(session):
    """
    Function to return dataframe that adds genre tags to the album .

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :return: the dataframe generated
    :rtype: :class:`pandas.DataFrame`
    """
    album_genre = session.query(Album.Title, Genre.Name).\
        select_from(Album).\
        join(Track).join(Genre).\
        group_by(Album.AlbumId, Genre.GenreId).\
        order_by(Album.AlbumId, Genre.GenreId).all()

    album_genre_df = pd.DataFrame(data=album_genre, columns=["Title", "Genre"])

    LOGGER.info(album_genre_df)

    return album_genre_df


@exception_handler
def pd_artist_genre(session):
    """
    Function to return dataframe that adds genre tags to the artist .

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :return: the dataframe generated
    :rtype: :class:`pandas.DataFrame`
    """
    artist_genre = session.query(Artist.Name, Genre.Name).\
        select_from(Artist).\
        join(Album).join(Track).join(Genre).\
        group_by(Artist.ArtistId, Genre.GenreId).\
        order_by(Artist.ArtistId, Genre.GenreId).all()

    artist_genre_df = pd.DataFrame(data=artist_genre, columns=["Name", "Genre"])

    LOGGER.info(artist_genre_df)

    return artist_genre_df
