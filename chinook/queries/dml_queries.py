# -- coding: UTF-8 --

"""
DML Queries
============
This module contains queries to the chinook database which alter the data in the tables
"""
import logging

from chinook.model.orm_classes import MediaType, Track, Employee, Customer, Invoice, Genre, Playlist, PlaylistTrack
from chinook.utils.error_handle import exception_handler, ERROR_MEDIA_ID_TYPE, ERROR_USER_TYPE, ERROR_PRICE_TYPE, \
    ERROR_CUSTOMER_ID_TYPE, ERROR_EMPLOYEE_ID_TYPE, ERROR_GENRE_ID_TYPE, ERROR_PLAYLIST_ID_TYPE

__author__ = 'srivathsan@gyandata.com'

LOGGER = logging.getLogger(__name__)


@exception_handler
def update_track_price_on_media_type(session, media_type_id, new_price, user="SYSTEM"):
    """
    Function to update the prices of tracks with given media type.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param media_type_id: the media type id
    :type media_type_id: int

    :param new_price: the price to which it must be updated
    :type new_price: Union(int, float)

    :param user: the user who does the soft delete, default SYSTEM.
    :type user: str

    :return: None
    :rtype: None
    """
    # parameter check
    if not issubclass(type(media_type_id), int):
        raise TypeError(ERROR_MEDIA_ID_TYPE)

    if not issubclass(type(user), str):
        raise TypeError(ERROR_USER_TYPE)

    if not issubclass(type(new_price), (int, float)):
        raise TypeError(ERROR_PRICE_TYPE)

    # check if customer id is valid
    _ = session.query(MediaType). \
        filter(MediaType.MediaTypeId == media_type_id).one()

    try:

        # get the tracks having this media type
        tracks = session.query(Track).filter(Track.MediaTypeId == media_type_id).all()

        # for each track update price and last updated by
        for track in tracks:

            LOGGER.info("Updating track %d for media type %d", track.TrackId, media_type_id)

            track.UnitPrice = new_price
            track.last_updated_by = user

        session.commit()

    except Exception as exp:

        session.rollback()
        raise exp


@exception_handler
def soft_delete_employee(session, employee_id, user="SYSTEM"):
    """
    Function to soft delete (disable) given employee.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param employee_id: the employee id
    :type employee_id: int

    :param user: the user who does the soft delete, default SYSTEM.
    :type user: str

    :return: None
    :rtype: None
    """
    # parameter check
    if not issubclass(type(employee_id), int):
        raise TypeError(ERROR_EMPLOYEE_ID_TYPE)

    if not issubclass(type(user), str):
        raise TypeError(ERROR_USER_TYPE)

    # check if customer id is valid
    _ = session.query(Employee). \
        filter(Employee.EmployeeId == employee_id).one()

    try:

        # get the employee
        emp = session.query(Employee).get(employee_id)

        LOGGER.info("Disabling employee %d", employee_id)

        # update is_enabled and last_updated_by column
        emp.is_enabled = False
        emp.last_updated_by = user

        session.commit()

    except Exception as exp:

        session.rollback()
        raise exp


@exception_handler
def delete_customer(session, customer_id):
    """
    Function to delete the given customer.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param customer_id: the customer id
    :type customer_id: int

    :return: None
    :rtype: None
    """
    # parameter check
    if not issubclass(type(customer_id), int):
        raise TypeError(ERROR_CUSTOMER_ID_TYPE)

    # check if customer id is valid
    _ = session.query(Customer). \
        filter(Customer.CustomerId == customer_id).one()

    try:
        LOGGER.debug("Before deleting customer %d, customer count: %d    customer-invoice count: %d",
                     customer_id, session.query(Customer).count(),
                     session.query(Invoice).filter(Invoice.CustomerId == customer_id).count())

        # delete given customer
        session.query(Customer).filter(Customer.CustomerId == customer_id).delete()

        LOGGER.debug("After deleting customer %d, customer count: %d    invoice count: %d",
                     customer_id, session.query(Customer).count(),
                     session.query(Invoice).filter(Invoice.CustomerId == customer_id).count())

        session.rollback()

        session.commit()

    except Exception as exp:

        session.rollback()
        raise exp


@exception_handler
def delete_genre(session, genre_id):
    """
    Function to delete the given genre.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param genre_id: the genre id
    :type genre_id: int

    :return: None
    :rtype: None
    """
    # parameter check
    if not issubclass(type(genre_id), int):
        raise TypeError(ERROR_GENRE_ID_TYPE)

    # check if genre id is valid
    _ = session.query(Genre). \
        filter(Genre.GenreId == genre_id).one()

    try:
        LOGGER.debug("Before deleting genre %d, genre count: %d    genre-track count: %d",
                     genre_id, session.query(Genre).count(),
                     session.query(Track).filter(Genre.GenreId == genre_id).count())

        # delete given genre
        session.query(Genre).filter(Genre.GenreId == genre_id).delete()

        LOGGER.debug("After deleting genre %d, genre count: %d    track count: %d",
                     genre_id, session.query(Genre).count(),
                     session.query(Track).filter(Genre.GenreId == genre_id).count())

        session.rollback()

        session.commit()

    except Exception as exp:

        session.rollback()
        raise exp


@exception_handler
def delete_playlist(session, playlist_id):
    """
    Function to delete the given playlist.

    :param session: the session used to queries
    :type session: :class:`sqlalchemy.orm.session.Session`

    :param playlist_id: the playlist id
    :type playlist_id: int

    :return: None
    :rtype: None
    """
    # parameter check
    if not issubclass(type(playlist_id), int):
        raise TypeError(ERROR_PLAYLIST_ID_TYPE)

    # check if playlist id is valid
    _ = session.query(Playlist). \
        filter(Playlist.PlaylistId == playlist_id).one()

    try:
        LOGGER.debug("Before deleting playlist %d, playlist count: %d    playlist-track count: %d",
                     playlist_id, session.query(Playlist).count(),
                     session.query(Playlist).join(PlaylistTrack).join(Track).\
                     filter(Playlist.PlaylistId == playlist_id).count())

        # delete given playlist
        session.query(Playlist).filter(Playlist.PlaylistId == playlist_id).delete()

        LOGGER.debug("After deleting playlist %d, playlist count: %d    playlist-track count: %d",
                     playlist_id, session.query(Playlist).count(),
                     session.query(Playlist).join(PlaylistTrack).join(Track).\
                     filter(Playlist.PlaylistId == playlist_id).count())

        session.rollback()

        session.commit()

    except Exception as exp:

        session.rollback()
        raise exp
