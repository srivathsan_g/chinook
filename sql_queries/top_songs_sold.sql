USE chinook;

SELECT 
	track.Name AS song_name,
    SUM(invoiceline.UnitPrice) AS Song
FROM
    track
        INNER JOIN
    invoiceline ON invoiceline.TrackId = track.TrackId
GROUP BY track.TrackId
ORDER BY Song DESC, track.TrackId ASC
LIMIT 10;