USE chinook;

SELECT 
    album.`Title` AS `Album`,
    COUNT(track.`TrackId`) AS `No_Songs`
FROM
    album
        INNER JOIN
    track ON album.`AlbumId` = track.`AlbumId`
GROUP BY album.`AlbumId`
ORDER BY No_Songs DESC , album.`AlbumId`
LIMIT 10