USE chinook;

SELECT 
    artist.`Name` AS `artist_Name`, genre.`Name` AS `genre_Name`
FROM
    artist
        INNER JOIN
    album ON artist.`ArtistId` = album.`ArtistId`
        INNER JOIN
    track ON album.`AlbumId` = track.`AlbumId`
        INNER JOIN
    genre ON genre.`GenreId` = track.`GenreId`
GROUP BY artist.`ArtistId` , genre.`GenreId`
ORDER BY artist.`ArtistId` , genre.`GenreId`
