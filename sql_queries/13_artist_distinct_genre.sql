USE chinook;

SELECT 
    artist.`Name` AS `Artist`,
    COUNT(genre.`GenreId`) AS `No_Genres`
FROM
    artist
        INNER JOIN
    album ON artist.`ArtistId` = album.`ArtistId`
        INNER JOIN
    track ON album.`AlbumId` = track.`AlbumId`
        INNER JOIN
    genre ON genre.`GenreId` = track.`GenreId`
WHERE
    genre.is_enabled = TRUE
GROUP BY artist.`ArtistId` , genre.`GenreId`
ORDER BY No_Genres DESC , artist.`ArtistId`
LIMIT 10;