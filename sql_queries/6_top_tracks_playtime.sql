USE chinook;

SELECT 
    track.`Name` AS `Song`, track.`Milliseconds`/1000 AS `Playtime`
FROM
    track
ORDER BY Playtime DESC
LIMIT 10;