USE chinook;

SELECT 
    customer.`FirstName` AS `Customer`,
    SUM(invoice.`Total`) AS `Amount`
FROM
    customer
        INNER JOIN
    invoice ON customer.`CustomerId` = invoice.`CustomerId`
WHERE
    invoice.is_enabled = TRUE
GROUP BY customer.`CustomerId`
ORDER BY Amount DESC , customer.`CustomerId`
LIMIT 10;