USE chinook;

select 
genre_name, song, no_purchases
from 
(
select
 genre.Name as genre_name, track.Name as song, count(invoiceline.InvoiceLineId) as `no_purchases`,
ROW_NUMBER() OVER (PARTITION BY track.GenreId ORDER BY track.GenreId, count(invoiceline.InvoiceLineId) DESC, track.TrackId) as genre_rank
from genre
inner join track on track.GenreId=genre.GenreId
inner join invoiceline on invoiceline.TrackId=track.TrackId
group by track.TrackId
) ranked_table
where genre_rank<=2