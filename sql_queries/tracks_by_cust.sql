USE chinook;

SELECT 
    playlist.`PlaylistId` AS `playlist_PlaylistId`,
    playlist.`Name` AS `playlist_Name`
FROM
    playlist
        INNER JOIN
    playlisttrack ON playlist.`PlaylistId` = playlisttrack.`PlaylistId`
        INNER JOIN
    track ON track.`TrackId` = playlisttrack.`TrackId`
        INNER JOIN
    invoiceline ON track.`TrackId` = invoiceline.`TrackId`
        INNER JOIN
    invoice ON invoice.`InvoiceId` = invoiceline.`InvoiceId`
WHERE
    invoice.`CustomerId` = 1;
    