USE chinook;

SELECT 
    artist.`Name` AS `Artist`,
    COUNT(track.`TrackId`) AS `No_Songs`
FROM
    artist
        INNER JOIN
    album ON artist.`ArtistId` = album.`ArtistId`
        INNER JOIN
    track ON album.`AlbumId` = track.`AlbumId`
WHERE
    track.is_enabled = TRUE
GROUP BY artist.`ArtistId`
ORDER BY No_Songs DESC , artist.`ArtistId`
LIMIT 10