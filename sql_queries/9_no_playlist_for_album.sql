USE chinook;

SELECT playlisttrack.PlaylistId
FROM
    playlisttrack
        INNER JOIN
    track ON track.`TrackId` = playlisttrack.`TrackId`
        INNER JOIN
    album ON album.`AlbumId` = track.`AlbumId`
WHERE	
    album.`AlbumId` = 10
GROUP BY album.AlbumId, playlisttrack.PlaylistId