USE chinook;

SELECT 
    employee.`FirstName` AS `Employee`,
    COUNT(invoice.`InvoiceId`) AS `Sales`
FROM
    employee
        INNER JOIN
    customer ON employee.`EmployeeId` = customer.`SupportRepId`
        INNER JOIN
    invoice ON customer.`CustomerId` = invoice.`CustomerId`
WHERE
    invoice.is_enabled = TRUE
        AND MONTH(invoice.`InvoiceDate`) = 1
        AND YEAR(invoice.`InvoiceDate`) = 2010
GROUP BY employee.`EmployeeId`
ORDER BY Sales DESC , employee.`EmployeeId`
LIMIT 10;