USE chinook;


SELECT 
    True
FROM
    track
        INNER JOIN
    genre ON track.GenreId = genre.GenreId
GROUP BY track.TrackId 
having count(track.GenreId)=all(select 1 from track)
