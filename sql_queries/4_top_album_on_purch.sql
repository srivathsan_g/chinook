USE chinook;

SELECT 
    album.`Title` AS `Album`,
    COUNT(track.TrackId) AS `No_Purchases`
FROM
    album
        INNER JOIN
    track ON album.`AlbumId` = track.`AlbumId`
WHERE
    track.TrackId = ANY (SELECT 
            track.TrackId
        FROM
            track
                INNER JOIN
            invoiceline ON track.`TrackId` = invoiceline.`TrackId`
        WHERE
            invoiceline.is_enabled = TRUE)
GROUP BY album.`AlbumId`
ORDER BY No_Purchases DESC , album.`AlbumId`