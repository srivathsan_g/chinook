USE chinook;

SELECT 
    album.`Title` AS `album_Title`, genre.`Name` AS `genre_Name`
FROM
    album
        INNER JOIN
    track ON album.`AlbumId` = track.`AlbumId`
        INNER JOIN
    genre ON genre.`GenreId` = track.`GenreId`
GROUP BY album.`AlbumId` , genre.`GenreId` having count(genre.GenreId)>1
ORDER BY album.`AlbumId` , genre.`GenreId`
