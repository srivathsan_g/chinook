USE chinook;

SELECT 
    COUNT(playlisttrack.`PlaylistId`) AS count_1
FROM
    playlisttrack
        INNER JOIN
    track ON track.`TrackId` = playlisttrack.`TrackId`
WHERE
    track.`TrackId` = 50;