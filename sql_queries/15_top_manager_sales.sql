USE chinook;

SELECT 
    employee_1.`FirstName` AS `Manager`,
    SUM(invoice.`Total`) AS `Sales`
FROM
    employee AS employee_1
        INNER JOIN
    employee ON employee_1.`EmployeeId` = employee.`ReportsTo`
        INNER JOIN
    customer ON employee.`EmployeeId` = customer.`SupportRepId`
        INNER JOIN
    invoice ON customer.`CustomerId` = invoice.`CustomerId`
WHERE
    invoice.is_enabled = TRUE
        AND MONTH(invoice.`InvoiceDate`) = 3
        AND YEAR(invoice.`InvoiceDate`) = 2013
GROUP BY employee_1.`EmployeeId`
ORDER BY Sales DESC , employee_1.`EmployeeId`
LIMIT 10;