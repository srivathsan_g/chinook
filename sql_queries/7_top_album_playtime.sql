USE chinook;

SELECT 
    album.`Title` AS `Album name`,
    SUM(track.`Milliseconds` / 1000) AS `Album_playtime`
FROM
    album
        INNER JOIN
    track ON album.`AlbumId` = track.`AlbumId`
GROUP BY album.`AlbumId`
ORDER BY Album_playtime DESC
LIMIT 10;