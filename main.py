# -- coding: UTF-8 --

"""
Chinook Main
==========
main script to test chinook music database
"""
import logging

from chinook.queries.dql_queries import get_all_tracks, get_tracks_of_playlists, \
    get_tracks_of_customer, get_playlists_having_tracks_bought_by_customer, \
    get_top_selling_tracks, get_top_albums_on_tracks, get_top_artists_on_tracks, get_top_cust_on_amount, \
    get_top_albums_on_purchase, get_tracks_on_purch_each_genre, get_no_playlist_for_track, get_top_tracks_playtime, \
    get_top_albums_tracks_playtime, get_no_playlist_for_album, genre_check_for_tracks, get_no_artist_for_dist_genre, \
    get_top_employee_sales, get_top_manager_sales
from chinook.queries.pd_queries import pd_album_genre, pd_artist_genre

from chinook.queries.dml_queries import delete_customer, delete_genre, delete_playlist, soft_delete_employee, \
    update_track_price_on_media_type

from chinook.queries.ddl_queries import alter_del_cascade

from chinook.utils.query_helpers import create_eng, get_session_factory
from chinook.utils.log import build_logger

__author__ = "srivathsan@gyandata.com"

LOG_CONFIG = "configs/log_config.json"

LOGGER = logging.getLogger(__name__)

DB_URL = "mysql+pymysql://root:patterns@localhost/chinook"


def main():
    """
    main function
    """
    build_logger(LOG_CONFIG)

    engine = create_eng(DB_URL, echo=True)

    session_factory = get_session_factory(engine)

    #alter_del_cascade(engine)

    session = session_factory()

    #get_all_tracks(session)

    #get_tracks_of_playlists(session, 1)

    #get_tracks_of_customer(session, 3)

    #get_playlists_having_tracks_bought_by_customer(session, 3)

    #get_top_selling_tracks(session, 10)

    #get_top_albums_on_tracks(session, 10)

    #get_top_artists_on_tracks(session, 10)

    #get_top_cust_on_amount(session, 10)

    #get_top_albums_on_purchase(session, 10)

    #get_tracks_on_purch_each_genre(session, 5)

    # get_top_tracks_playtime(session, 10)

    #get_top_albums_tracks_playtime(session, 10)

    #get_no_playlist_for_track(session, 50)

    #get_no_playlist_for_album(session, 1)

    #genre_check_for_tracks(session)

    #pd_album_genre(session)

    #pd_artist_genre(session)

    #get_no_artist_for_dist_genre(session, 10)

    #get_top_employee_sales(session, 10, 'January', 2010)

    #get_top_manager_sales(session, 10, 3, 2013)

    #delete_customer(session, customer_id=1)

    #delete_genre(session, genre_id=3)

    #delete_playlist(session, playlist_id=1)

    #soft_delete_employee(session, 1, "root")

    #update_track_price_on_media_type(session, 1, 1.5, "root")




if __name__ == '__main__':
    main()
